# ledc-core

## Intro

This repository is part of ledc, which is short for *Lightweight Engine for Data quality Control*. 
It is a modular framework designed for data quality monitoring and improvement in several different ways.
In the current repository, you can find ledc-core: a general purpose library that bundles functionality shared by the different ledc modules.
The module basically contains two main types of utilities:

* **Data model**: ledc uses a simple custom data model to work with.
The central entity in this model is a *DataObject*, which is a simple key-value datastructure.
DataObjects are bundled in a *Dataset* that comes with a set of operations for easy manipulation of large sets of DataObjects.
There is support for basic binding to PostgreSQL and .csv files.

* **Operators**: across the different ledc modules, several basic operators are used over and over again.
Typical examples include aggregation operators and different types of metrics (in the strict sense).
The core module centralizes the definitions and implementations of these operators.

We should emphasize here that ledc-core is by no means intended to be complete in any of both listed aspects.
It lives to support the other modules and that's basically it.
Additional functionality can be added (if deemed necessary) in the future.