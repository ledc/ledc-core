package be.ugent.ledc.core.config;

public interface Persistable
{
    public FeatureMap buildFeatureMap();

}
