package be.ugent.ledc.core.cost;

import be.ugent.ledc.core.dataset.DataObject;
import java.util.HashSet;
import java.util.Set;

public interface CostFunction<T>
{
    int cost(T originalValue, T repairedValue, DataObject originalObject);
    
    default Set<T> alternatives(T originalValue, DataObject originalObject)
    {
        return new HashSet<>();
    }
}
