package be.ugent.ledc.core.cost;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.RepairRuntimeException;

public class ConstantCostFunction<T> implements CostFunction<T>
{
    private final int fixedCost;

    public ConstantCostFunction()
    {
        this.fixedCost = 1;
    }
    
    public ConstantCostFunction(int fixedCost)
    {
        this.fixedCost = fixedCost;
        
        if(fixedCost <= 0)
            throw new RepairRuntimeException("Cost functions must be positive-definite: fixed cost must be strictly greater than 0.");
    }

    @Override
    public int cost(T originalValue, T repairedValue, DataObject originalObject)
    {
        if(originalValue == null)
            return fixedCost;
        
        if(repairedValue == null)
            return Integer.MAX_VALUE;
                    
        if(originalValue.equals(repairedValue))
            return 0;
        
        return fixedCost;
    }

    public int getFixedCost()
    {
        return fixedCost;
    }
}
