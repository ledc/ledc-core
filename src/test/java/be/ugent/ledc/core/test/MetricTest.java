package be.ugent.ledc.core.test;

import be.ugent.ledc.core.operators.metric.DamerauDistance;
import be.ugent.ledc.core.operators.metric.LevenshteinDistance;
import be.ugent.ledc.core.operators.metric.ReMeDistance;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Contains a series of unit tests to verify the proper functionality of metrics
 *
 * @author abronsel
 */
public class MetricTest {

    public MetricTest() {
    }

    @Test
    public void LevenshteinInsertionTest() {
        assertEquals(2, new LevenshteinDistance().distance("Test string", "Test stringer").intValue());
    }

    @Test
    public void LevenshteinDeletionTest() {
        assertEquals(2, new LevenshteinDistance().distance("Test string", "Tes strng").intValue());
    }

    @Test
    public void LevenshteinSubstitutionTest() {
        assertEquals(3, new LevenshteinDistance().distance("Test string", "Post strong").intValue());
    }

    @Test
    public void LevenshteinComboTest() {
        assertEquals(6, new LevenshteinDistance().distance("This is a test string", "this is a toast stronger").intValue());
    }

    @Test
    public void DamerauInsertionTest() {
        assertEquals(2, new DamerauDistance().distance("Test string", "Test stringer").intValue());
    }

    @Test
    public void DamerauDeletionTest() {
        assertEquals(2, new DamerauDistance().distance("Test string", "Tes strng").intValue());
    }

    @Test
    public void DamerauSubstitutionTest() {
        assertEquals(3, new DamerauDistance().distance("Test string", "Post strong").intValue());
    }

    @Test
    public void DamerauTranspositionTest() {
        assertEquals(1, new DamerauDistance().distance("Test string", "Test stirng").intValue());
    }

    @Test
    public void DamerauComboTest() {
        assertEquals(6, new DamerauDistance().distance("This is a test string", "this is a toast stronger").intValue());
    }

    @Test
    public void remeMatcherTest1() {
        ReMeDistance rem = new ReMeDistance();

        Integer d = rem.distance("ee", "e");

        assertEquals(1, d.intValue());
    }
    
    @Test
    public void remeMatcherTest2() {
        ReMeDistance rem = new ReMeDistance();

        Integer d = rem.distance("eee", "ee");

        assertEquals(1, d.intValue());
    }
    
    @Test
    public void remeMatcherTest3() {
        ReMeDistance rem = new ReMeDistance();

        Integer d = rem.distance("", "e");

        assertEquals(2, d.intValue());
    }
    
    @Test
    public void remeMatcherTest4() {
        ReMeDistance rem = new ReMeDistance();

        Integer d = rem.distance("Jeffrey", "Jefrey");

        assertEquals(1, d.intValue());
    }
    
    @Test
    public void remeMatcherTest5() {
        ReMeDistance rem = new ReMeDistance();

        Integer d = rem.distance("Jefrey", "Jeffrey");

        assertEquals(1, d.intValue());
    }
    
    @Test
    public void remeMatcherTest6() {
        ReMeDistance rem = new ReMeDistance();

        Integer d = rem.distance("Jefrey", "Jeffrreey");

        assertEquals(3, d.intValue());
    }
}
