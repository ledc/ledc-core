package be.ugent.ledc.core.test;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.postgresql.util.PGInterval;

public class TypeContractorsTest
{
    @Test
    public void stringNameTest()
    {
        Assert.assertEquals("string", TypeContractorFactory.STRING.name());
    }
    
    @Test
    public void stringPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.STRING.test("5"));
        Assert.assertEquals(false, TypeContractorFactory.STRING.test(5));
    }
    
    @Test
    public void stringGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", "5");
        assertEquals("5", TypeContractorFactory.STRING.getFromDataObject(o, "a"));
    }
    
    @Test
    public void integerNameTest()
    {
        Assert.assertEquals("integer", TypeContractorFactory.INTEGER.name());
    }
    
    @Test
    public void integerPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.INTEGER.test(5));
        Assert.assertEquals(false, TypeContractorFactory.INTEGER.test("5"));
    }
    
    @Test
    public void integerGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", 5);
        assertEquals(Integer.valueOf(5), TypeContractorFactory.INTEGER.getFromDataObject(o, "a"));
    }
    
    @Test
    public void longNameTest()
    {
        Assert.assertEquals("long", TypeContractorFactory.LONG.name());
    }
    
    @Test
    public void longPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.LONG.test(5L));
        Assert.assertEquals(false, TypeContractorFactory.LONG.test("5"));
    }
    
    @Test
    public void longGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", 5L);
        assertEquals(Long.valueOf(5), TypeContractorFactory.LONG.getFromDataObject(o, "a"));
    }
    
    @Test
    public void booleanNameTest()
    {
        Assert.assertEquals("boolean", TypeContractorFactory.BOOLEAN.name());
    }
    
    @Test
    public void booleanPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.BOOLEAN.test(true));
        Assert.assertEquals(false, TypeContractorFactory.BOOLEAN.test("true"));
    }
    
    @Test
    public void booleanGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", true);
        assertEquals(true, TypeContractorFactory.BOOLEAN.getFromDataObject(o, "a"));
    }
    
    @Test
    public void bigdecimalNameTest()
    {
        Assert.assertEquals("bigdecimal", TypeContractorFactory.BIGDECIMAL.name());
    }
    
    @Test
    public void bigdecimalPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.BIGDECIMAL.test(BigDecimal.valueOf(0.5)));
        Assert.assertEquals(false, TypeContractorFactory.BIGDECIMAL.test("0.5"));
    }
    
    @Test
    public void bigdecimalGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", new BigDecimal("0.553"));
        assertEquals(new BigDecimal("0.553"), TypeContractorFactory.BIGDECIMAL.getFromDataObject(o, "a"));
    }
    
    @Test
    public void floatNameTest()
    {
        Assert.assertEquals("float", TypeContractorFactory.FLOAT.name());
    }
    
    @Test
    public void floatPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.FLOAT.test(0.5f));
        Assert.assertEquals(false, TypeContractorFactory.FLOAT.test("0.5f"));
    }
    
    @Test
    public void floatGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", 0.5f);
        assertEquals(Float.valueOf("0.5f"), TypeContractorFactory.FLOAT.getFromDataObject(o, "a"));
    }
   
    @Test
    public void doubleNameTest()
    {
        Assert.assertEquals("double", TypeContractorFactory.DOUBLE.name());
    }
    
    @Test
    public void doublePredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.DOUBLE.test(0.5));
        Assert.assertEquals(false, TypeContractorFactory.DOUBLE.test("0.5"));
    }
    
    @Test
    public void doubleGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", 0.5);
        assertEquals(Double.valueOf("0.5"), TypeContractorFactory.DOUBLE.getFromDataObject(o, "a"));
    }
    
    @Test
    public void dateNameTest()
    {
        Assert.assertEquals("date", TypeContractorFactory.DATE.name());
    }
    
    @Test
    public void datePredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.DATE.test(LocalDate.of(2012, Month.OCTOBER, 20)));
        Assert.assertEquals(false, TypeContractorFactory.DATE.test("10/10/2013"));
    }
    
    @Test
    public void dateGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", LocalDate.of(2012, Month.OCTOBER, 20));
        assertEquals(LocalDate.of(2012, Month.OCTOBER, 20), TypeContractorFactory.DATE.getFromDataObject(o, "a"));
    }
    
    @Test
    public void timeNameTest()
    {
        Assert.assertEquals("time", TypeContractorFactory.TIME.name());
    }
    
    @Test
    public void timePredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.TIME.test(LocalTime.of(10, 10, 20)));
        Assert.assertEquals(false, TypeContractorFactory.TIME.test("10:00:20"));
    }
    
    @Test
    public void timeGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", LocalTime.of(10, 10, 20));
        assertEquals(LocalTime.of(10, 10, 20), TypeContractorFactory.TIME.getFromDataObject(o, "a"));
    }
    
    public void datetimeNameTest()
    {
        Assert.assertEquals("datetime", TypeContractorFactory.DATETIME.name());
    }
    
    @Test
    public void datetimePredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.DATETIME.test(LocalDateTime.of(2012, Month.OCTOBER, 20, 10, 10, 20)));
        Assert.assertEquals(false, TypeContractorFactory.DATETIME.test(1056));
    }
    
    @Test
    public void datetimeGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", LocalDateTime.of(2012, Month.OCTOBER, 20, 10, 10, 20));
        assertEquals(LocalDateTime.of(2012, Month.OCTOBER, 20, 10, 10, 20), TypeContractorFactory.DATETIME.getFromDataObject(o, "a"));
    }
    
    public void durationNameTest()
    {
        Assert.assertEquals("duration", TypeContractorFactory.DURATION.name());
    }
    
    @Test
    public void durationPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.DURATION.test(Duration.ofDays(5)));
        Assert.assertEquals(false, TypeContractorFactory.DURATION.test(1056));
    }
    
    @Test
    public void durationGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", Duration.ofDays(5));
        assertEquals(Duration.ofDays(5), TypeContractorFactory.DURATION.getFromDataObject(o, "a"));
    }
    
    public void instantNameTest()
    {
        Assert.assertEquals("instant", TypeContractorFactory.INSTANT.name());
    }
    
    @Test
    public void instantPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.INSTANT.test(Instant.now()));
        Assert.assertEquals(false, TypeContractorFactory.INSTANT.test(Duration.ofDays(5)));
    }
    
    @Test
    public void instantGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", Instant.parse("2020-10-10T10:00:10.05Z"));
        assertEquals(Instant.parse("2020-10-10T10:00:10.05Z"), TypeContractorFactory.INSTANT.getFromDataObject(o, "a"));
    }
    
    public void listNameTest()
    {
        Assert.assertEquals("list", TypeContractorFactory.LIST.name());
    }
    
    @Test
    public void listPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.LIST.test(Stream.of(1,2,3).collect(Collectors.toList())));
        Assert.assertEquals(false, TypeContractorFactory.LIST.test(new HashSet<>()));
    }
    
    @Test
    public void listGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", Stream.of(1,2,3).collect(Collectors.toList()));
        assertEquals(Stream.of(1,2,3).collect(Collectors.toList()), TypeContractorFactory.LIST.getFromDataObject(o, "a"));
    }
    
    public void dataObjectNameTest()
    {
        Assert.assertEquals("dataobject", TypeContractorFactory.DATA_OBJECT.name());
    }
    
    @Test
    public void dataObjectPredicateTest()
    {
        Assert.assertEquals(true, TypeContractorFactory.DATA_OBJECT.test(new DataObject().set("b", 1)));
        Assert.assertEquals(false, TypeContractorFactory.DATA_OBJECT.test(new HashSet<>()));
    }
    
    @Test
    public void dataObjectGetFromObjectTest()
    {
        DataObject o = new DataObject().set("a", new DataObject().set("b", 1));
        assertEquals(new DataObject().set("b", 1), TypeContractorFactory.DATA_OBJECT.getFromDataObject(o, "a"));
    }
    
    @Test
    public void unboundTest()
    {
        DataObject o = new DataObject().set(
            "pginterval",
            new PGInterval(2, 3, 6, 8, 28, 3)
        );
        
        Object p = TypeContractorFactory.OBJECT.getFromDataObject(o, "pginterval");
        
        assertTrue(p instanceof PGInterval);
        
    }
}
