package be.ugent.ledc.core.test;

import be.ugent.ledc.core.datastructures.Interval;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Contains a series of unit tests to verify the basic functionalities of open and closed intervals
 * @author abronsel
 */
public class IntervalTest
{
    @Test
    public void openTest()
    {
        Interval<Integer> i = new Interval<>(2,3);
        
        assertEquals(true, i.isLeftOpen());
        assertEquals(true, i.isRightOpen());
    }
    
    @Test
    public void nullBoundOpenTest()
    {
        Interval<Integer> i = new Interval<>(null,null, false, false);
        
        assertEquals(true, i.isLeftOpen());
        assertEquals(true, i.isRightOpen());
        assertEquals(true, i.isOpen());
    }
    
    @Test
    public void closedTest()
    {
        Interval<Integer> i = new Interval<>(2,3, false, false);
        
        assertEquals(false, i.isLeftOpen());
        assertEquals(false, i.isRightOpen());
    }
    
    @Test
    public void leftBoundComparatorTest()
    {
        //Create some intervals
        Interval<Integer> i5 = new Interval<>(null,3,false, false);
        Interval<Integer> i1 = new Interval<>(2,3,false, false);
        Interval<Integer> i2 = new Interval<>(3,8,false, false);
        Interval<Integer> i3 = new Interval<>(5,8);
        Interval<Integer> i4 = new Interval<>(5,null, false, false);
        Interval<Integer> i6 = new Interval<>(2,3,true, false);
        
        //Sort them with left bound comparator
        List<Interval<Integer>> sortedByLeftBound = Stream
        .of(i1,i2,i3,i4,i5,i6)
        .sorted(Interval.leftBoundComparator())
        .collect(Collectors.toList());
        
        assertEquals(i5, sortedByLeftBound.get(0));
        assertEquals(i1, sortedByLeftBound.get(1));
        assertEquals(i6, sortedByLeftBound.get(2));
        assertEquals(i2, sortedByLeftBound.get(3));
        assertEquals(i4, sortedByLeftBound.get(4));
        assertEquals(i3, sortedByLeftBound.get(5));
        
    }
   
    @Test
    public void allenBeforeTestRightClosed()
    {
        //Interval [5,6]
        Interval<Integer> i  = new Interval<>(5, 6, false, false);
        
        //Interval [6, ...[
        Interval<Integer> i2 = new Interval<>(6, null, false, true);
        //Interval ]6, ...[
        Interval<Integer> i3 = new Interval<>(6, null, true, true);
        //Interval [7, ...[
        Interval<Integer> i4 = new Interval<>(7, null, false, true);

        assertEquals(false, i.allenBefore(i2));
        assertEquals(true, i.allenBefore(i3));
        assertEquals(true, i.allenBefore(i4));
    }
    
    @Test
    public void allenBeforeTestRightOpen()
    {
        //Interval [5,6]
        Interval<Integer> i  = new Interval<>(5, 6, false, true);
        
        //Interval [6, ...[
        Interval<Integer> i2 = new Interval<>(6, null, false, true);
        //Interval ]6, ...[
        Interval<Integer> i3 = new Interval<>(6, null, true, true);
        //Interval [7, ...[
        Interval<Integer> i4 = new Interval<>(7, null, false, true);

        assertEquals(true, i.allenBefore(i2));
        assertEquals(true, i.allenBefore(i3));
        assertEquals(true, i.allenBefore(i4));
    }
    
    @Test
    public void allenMeetsTestRightClosed()
    {
        //Interval [5,6]
        Interval<Integer> i  = new Interval<>(5, 6, false, false);
        
        //Interval [6, ...[
        Interval<Integer> i2 = new Interval<>(6, null, false, true);
        //Interval ]6, ...[
        Interval<Integer> i3 = new Interval<>(6, null, true, true);
        //Interval [7, ...[
        Interval<Integer> i4 = new Interval<>(7, null, false, true);

        assertEquals(true, i.allenMeets(i2));
        assertEquals(false, i.allenMeets(i3));
        assertEquals(false, i.allenMeets(i4));
    }
    
    @Test
    public void allenMeetsTestRightOpen()
    {
        //Interval [5,6]
        Interval<Integer> i  = new Interval<>(5, 6, false, true);
        
        //Interval [6, ...[
        Interval<Integer> i2 = new Interval<>(6, null, false, true);
        //Interval ]6, ...[
        Interval<Integer> i3 = new Interval<>(6, null, true, true);
        //Interval [7, ...[
        Interval<Integer> i4 = new Interval<>(7, null, false, true);

        assertEquals(false, i.allenMeets(i2));
        assertEquals(false, i.allenMeets(i3));
        assertEquals(false, i.allenMeets(i4));
    }
    
    @Test
    public void allenOverlapsTestRightClosed()
    {
        //Interval [5,8]
        Interval<Integer> i  = new Interval<>(5, 8, false, false);
        
        //Interval [8, ...[
        Interval<Integer> i2 = new Interval<>(8, null, false, true);
        //Interval ]8, ...[
        Interval<Integer> i3 = new Interval<>(8, null, true, true);
        //Interval [7, ...[
        Interval<Integer> i4 = new Interval<>(7, null, false, true);

        assertEquals(false, i.allenOverlaps(i2));
        assertEquals(false, i.allenOverlaps(i3));
        assertEquals(true, i.allenOverlaps(i4));
    }
    
    @Test
    public void allenOverlapsTestRightOpen()
    {
        //Interval [5,8[
        Interval<Integer> i  = new Interval<>(5, 8, false, true);
        
        //Interval [8, ...[
        Interval<Integer> i2 = new Interval<>(8, null, false, true);
        //Interval ]8, ...[
        Interval<Integer> i3 = new Interval<>(8, null, true, true);
        //Interval [7, ...[
        Interval<Integer> i4 = new Interval<>(7, null, false, true);

        assertEquals(false, i.allenOverlaps(i2));
        assertEquals(false, i.allenOverlaps(i3));
        assertEquals(true, i.allenOverlaps(i4));
    }
    
    @Test
    public void allenStartsTestLeftClosed()
    {
        //Interval [5,8]
        Interval<Integer> i  = new Interval<>(5, 8, false, false);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        //Interval [5,8[
        Interval<Integer> i3 = new Interval<>(5, 8, false, true);
        //Interval [5,9[
        Interval<Integer> i4 = new Interval<>(5, 9, false, true);
        //Interval ]5,9[
        Interval<Integer> i5 = new Interval<>(5, 9, true, true);
        

        assertEquals(false, i.allenStarts(i2));
        assertEquals(false, i.allenStarts(i3));
        assertEquals(true, i.allenStarts(i4));
        assertEquals(false, i.allenStarts(i5));
    }
    
    @Test
    public void allenStartsTestLeftOpen()
    {
        //Interval ]5,8]
        Interval<Integer> i  = new Interval<>(5, 8, true, false);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        //Interval [5,8[
        Interval<Integer> i3 = new Interval<>(5, 8, false, true);
        //Interval [5,9[
        Interval<Integer> i4 = new Interval<>(5, 9, false, true);
        //Interval ]5,9[
        Interval<Integer> i5 = new Interval<>(5, 9, true, true);
        

        assertEquals(false, i.allenStarts(i2));
        assertEquals(false, i.allenStarts(i3));
        assertEquals(false, i.allenStarts(i4));
        assertEquals(true, i.allenStarts(i5));
    }
    
    @Test
    public void allenDuringTestLeftClosedRightClosed()
    {
        //Interval [5,8]
        Interval<Integer> i  = new Interval<>(5, 8, false, false);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        
        //Interval ]5,8[
        Interval<Integer> i3 = new Interval<>(5, 8, true, true);
        
        //Interval [4,9]
        Interval<Integer> i4 = new Interval<>(4, 9, false, false);
        //Interval [5,9]
        Interval<Integer> i5 = new Interval<>(5, 9, false, false);
        //Interval [6,8]
        Interval<Integer> i6 = new Interval<>(6, 8, false, false);
        

        assertEquals(false, i.allenDuring(i2));
        assertEquals(false, i.allenDuring(i3));
        assertEquals(true, i.allenDuring(i4));
        assertEquals(false, i.allenDuring(i5));
        assertEquals(false, i.allenDuring(i6));
    }
    
    @Test
    public void allenDuringTestLeftClosedRightOpen()
    {
        //Interval [5,8[
        Interval<Integer> i  = new Interval<>(5, 8, false, true);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        
        //Interval ]5,8[
        Interval<Integer> i3 = new Interval<>(5, 8, true, true);
        
        //Interval [4,9]
        Interval<Integer> i4 = new Interval<>(4, 9, false, false);
        //Interval [5,9]
        Interval<Integer> i5 = new Interval<>(5, 9, false, false);
        //Interval [6,8]
        Interval<Integer> i6 = new Interval<>(6, 8, false, false);
        

        assertEquals(false, i.allenDuring(i2));
        assertEquals(false, i.allenDuring(i3));
        assertEquals(true, i.allenDuring(i4));
        assertEquals(false, i.allenDuring(i5));
        assertEquals(false, i.allenDuring(i6));
    }
    
    @Test
    public void allenDuringTestLeftOpenRightClosed()
    {
        //Interval ]5,8]
        Interval<Integer> i  = new Interval<>(5, 8, true, false);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        
        //Interval ]5,8[
        Interval<Integer> i3 = new Interval<>(5, 8, true, true);
        
        //Interval [4,9]
        Interval<Integer> i4 = new Interval<>(4, 9, false, false);
        //Interval [5,9]
        Interval<Integer> i5 = new Interval<>(5, 9, false, false);
        //Interval [6,8]
        Interval<Integer> i6 = new Interval<>(6, 8, false, false);
        

        assertEquals(false, i.allenDuring(i2));
        assertEquals(false, i.allenDuring(i3));
        assertEquals(true, i.allenDuring(i4));
        assertEquals(true, i.allenDuring(i5));
        assertEquals(false, i.allenDuring(i6));
    }
    
    @Test
    public void allenDuringTestLeftOpenRightOpen()
    {
        //Interval ]5,8[
        Interval<Integer> i  = new Interval<>(5, 8, true, true);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        
        //Interval ]5,8[
        Interval<Integer> i3 = new Interval<>(5, 8, true, true);
        
        //Interval [4,9]
        Interval<Integer> i4 = new Interval<>(4, 9, false, false);
        //Interval [5,9]
        Interval<Integer> i5 = new Interval<>(5, 9, false, false);
        //Interval [6,8]
        Interval<Integer> i6 = new Interval<>(6, 8, false, false);
        

        assertEquals(true, i.allenDuring(i2));
        assertEquals(false, i.allenDuring(i3));
        assertEquals(true, i.allenDuring(i4));
        assertEquals(true, i.allenDuring(i5));
        assertEquals(false, i.allenDuring(i6));
    }
    
    @Test
    public void allenFinishedTestRightClosed()
    {
        //Interval [5,8]
        Interval<Integer> i  = new Interval<>(5, 8, false, false);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        //Interval ]5,8]
        Interval<Integer> i3 = new Interval<>(5, 8, true, false);
        //Interval ]5,8[
        Interval<Integer> i4 = new Interval<>(5, 8, true, true);
        //Interval [3,8]
        Interval<Integer> i5 = new Interval<>(3, 8, false, false);
        

        assertEquals(false, i.allenFinishes(i2));
        assertEquals(false, i.allenFinishes(i3));
        assertEquals(false, i.allenFinishes(i4));
        assertEquals(true, i.allenFinishes(i5));
    }
    
    @Test
    public void allenFinishedTestRightOpen()
    {
        //Interval [5,8[
        Interval<Integer> i  = new Interval<>(5, 8, false, true);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        //Interval ]5,8]
        Interval<Integer> i3 = new Interval<>(5, 8, true, false);
        //Interval ]4,8[
        Interval<Integer> i4 = new Interval<>(4, 8, true, true);
        //Interval [3,8]
        Interval<Integer> i5 = new Interval<>(3, 8, false, false);
        

        assertEquals(false, i.allenFinishes(i2));
        assertEquals(false, i.allenFinishes(i3));
        assertEquals(true, i.allenFinishes(i4));
        assertEquals(false, i.allenFinishes(i5));
    }
    
    @Test
    public void allenEqualsTest()
    {
        //Interval [5,8]
        Interval<Integer> i  = new Interval<>(5, 8, false, false);
        
        //Interval [5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, false, false);
        //Interval ]5,8]
        Interval<Integer> i3 = new Interval<>(5, 8, true, false);
        //Interval [5,8[
        Interval<Integer> i4 = new Interval<>(4, 8, false, true);
        //Interval ]5,8[
        Interval<Integer> i5 = new Interval<>(3, 8, true, true);
        

        assertEquals(true, i.allenEquals(i2));
        assertEquals(false, i.allenEquals(i3));
        assertEquals(false, i.allenEquals(i4));
        assertEquals(false, i.allenEquals(i5));
    }
    
    @Test
    public void allenEqualsNullBoundsTest()
    {
        //Interval [5,...[
        Interval<Integer> i  = new Interval<>(5, null, false, false);
        
        //Interval [5,...[
        Interval<Integer> i2 = new Interval<>(5, null, false, true);
        //Interval ]5,8]
        Interval<Integer> i3 = new Interval<>(5, 8, true, false);
        //Interval [5,8[
        Interval<Integer> i4 = new Interval<>(4, 8, false, true);
        //Interval ]5,8[
        Interval<Integer> i5 = new Interval<>(3, 8, true, true);
        

        assertEquals(true, i.allenEquals(i2));
        assertEquals(false, i.allenEquals(i3));
        assertEquals(false, i.allenEquals(i4));
        assertEquals(false, i.allenEquals(i5));
    }
    
    @Test
    public void pointContainmentTest()
    {
        
        //Interval [5,8]
        Interval<Integer> i1 = new Interval<>(5, 8, false, false);
        //Interval ]5,8]
        Interval<Integer> i2 = new Interval<>(5, 8, true, false);
        //Interval [5,8[
        Interval<Integer> i3 = new Interval<>(5, 8, false, true);
        
        assertEquals(true, i1.contains(5));
        assertEquals(true, i1.contains(8));
        assertEquals(false, i2.contains(5));
        assertEquals(true, i2.contains(8));
        assertEquals(true, i3.contains(5));
        assertEquals(false, i3.contains(8));
    }
    
    @Test
    public void disjunctTest()
    {
        
        //Interval [5,8]
        Interval<Integer> left1 = new Interval<>(5, 8, false, false);
        //Interval [5,8[
        Interval<Integer> left2 = new Interval<>(5, 8, false, true);
        
        //Interval [8,10]
        Interval<Integer> right1 = new Interval<>(8, 10, false, false);
        
        //Interval ]8,10]
        Interval<Integer> right2 = new Interval<>(8, 10, true, false);
        
        assertEquals(false, left1.isDisjunctWith(right1));
        assertEquals(true, left1.isDisjunctWith(right2));
        assertEquals(true, left2.isDisjunctWith(right1));
        assertEquals(true, left2.isDisjunctWith(right2));
    }
}
