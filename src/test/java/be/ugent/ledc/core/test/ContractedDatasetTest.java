package be.ugent.ledc.core.test;

import be.ugent.ledc.core.dataset.AggregationClause;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.util.SetOperations;

import java.time.LocalDate;
import java.time.Month;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ContractedDatasetTest
{
    @Test
    public void naturalJoinTest()
    {
        Contract leftContract = new Contract.ContractBuilder()
            .addContractor("a", TypeContractorFactory.STRING)
            .addContractor("b", TypeContractorFactory.STRING)
            .build();
        
        Contract rightContract = new Contract.ContractBuilder()
            .addContractor("b", TypeContractorFactory.STRING)
            .addContractor("c", TypeContractorFactory.STRING)
            .build();
        
        Contract joinContract = new Contract.ContractBuilder()
            .addContractor("a", TypeContractorFactory.STRING)
            .addContractor("b", TypeContractorFactory.STRING)
            .addContractor("c", TypeContractorFactory.STRING)
            .build();
        
        ContractedDataset left = new ContractedDataset(leftContract);
        ContractedDataset right = new ContractedDataset(rightContract);
        ContractedDataset join = new ContractedDataset(joinContract);
        
        //Add left
        left.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setString("b", "1"));

        left.addDataObject(
            new DataObject()
                .setString("a", "2")
                .setString("b", "1"));
        
        left.addDataObject(
            new DataObject()
                .setString("a", "3")
                .setString("b", "1"));
        
        left.addDataObject(
            new DataObject()
                .setString("a", "2")
                .setString("b", "2"));
        
        //Add right
        right.addDataObject(
            new DataObject()
                .setString("b", "2")
                .setString("c", "2"));
        
        right.addDataObject(
            new DataObject()
                .setString("b", "1")
                .setString("c", "5"));
        
        //Add to join
        join.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setString("b", "1")
                .setString("c", "5"));
        join.addDataObject(
            new DataObject()
                .setString("a", "2")
                .setString("b", "1")
                .setString("c", "5"));
        join.addDataObject(
            new DataObject()
                .setString("a", "3")
                .setString("b", "1")
                .setString("c", "5"));
        join.addDataObject(
            new DataObject()
                .setString("a", "2")
                .setString("b", "2")
                .setString("c", "2"));
        
        assertEquals(join.getDataObjects(), left.naturalJoin(right).getDataObjects());
    }
    
    @Test
    public void groupByTestAggregateInteger()
    {
        Contract contract = new Contract.ContractBuilder()
            .addContractor("a", TypeContractorFactory.STRING)
            .addContractor("b", TypeContractorFactory.INTEGER)
            .build();
        
        ContractedDataset dataset = new ContractedDataset(contract);
        SimpleDataset grouped = new SimpleDataset();
        
        //Add left
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setInteger("b", 1)
        );
        
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setInteger("b", 5)
        );
        
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setInteger("b", 3)
        );
        
        grouped.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setInteger("sum_of_b", 9)
        );
        
        assertEquals(grouped, dataset.groupByAndAggregate(
            Stream.of("a").collect(Collectors.toSet()),
            new AggregationClause<>(
                "b",
                "sum_of_b",
                (List<Integer> values) -> values.stream().mapToInt(i->i).sum()
            )
        ));
    }
    
    @Test
    public void groupByTestAggregateDate()
    {
        Contract contract = new Contract.ContractBuilder()
            .addContractor("a", TypeContractorFactory.STRING)
            .addContractor("b", TypeContractorFactory.DATE)
            .build();
        
        ContractedDataset dataset = new ContractedDataset(contract);
        SimpleDataset grouped = new SimpleDataset();
        
        //Add left
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("b", LocalDate.of(2021, Month.OCTOBER, 26))
        );
        
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("b", LocalDate.of(2020, Month.MAY, 31))
        );
        
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("b", LocalDate.of(2022, Month.JULY, 29))
        );
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("b", LocalDate.of(2019, Month.JUNE, 20))
        );
        
        grouped.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("max_of_b", LocalDate.of(2022, Month.JULY, 29))
        );
        
        assertEquals(grouped, dataset.groupByAndAggregate(
            Stream.of("a").collect(Collectors.toSet()),
            new AggregationClause<>(
                "b",
                "max_of_b",
                (List<LocalDate> values) -> values.stream().max(Comparator.naturalOrder()).get()
            )
        ));
    }
    
    @Test
    public void conversionTest()
    {
        Contract contract = new Contract.ContractBuilder()
            .addContractor("a", TypeContractorFactory.STRING)
            .addContractor("b", TypeContractorFactory.DATE)
            .build();
        
        ContractedDataset dataset = new ContractedDataset(contract);

        //Add left
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("b", LocalDate.of(2021, Month.OCTOBER, 26))
        );
        
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("b", LocalDate.of(2020, Month.MAY, 31))
        );
        
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("b", LocalDate.of(2022, Month.JULY, 29))
        );
        dataset.addDataObject(
            new DataObject()
                .setString("a", "1")
                .setDate("b", LocalDate.of(2019, Month.JUNE, 20))
        );

        assertEquals(TypeContractorFactory.INTEGER, dataset
            .asInteger("a")
            .getContract()
            .getAttributeContract("a"));
    }
    
    @Test
    public void multiCastToIntTest()
    {
        ContractedDataset dataset = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("a", TypeContractorFactory.STRING)
            .addContractor("b", TypeContractorFactory.STRING)
            .build()
        );
        
        dataset.addDataObject(new DataObject()
            .set("a", "8")
            .set("b", "9"));
        dataset.addDataObject(new DataObject()
            .set("a", "9")
            .set("b", "10"));
        
        ContractedDataset expected = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("a", TypeContractorFactory.INTEGER)
            .addContractor("b", TypeContractorFactory.INTEGER)
            .build()
        );
        
        expected.addDataObject(new DataObject()
            .set("a", 8)
            .set("b", 9));
        expected.addDataObject(new DataObject()
            .set("a", 9)
            .set("b", 10));
        
        ContractedDataset casted = dataset
            .asInteger("a")
            .asInteger("b");
        
        assertEquals(expected, casted);
    }
    
    @Test
    public void multiCastToIntSinglePassTest()
    {
        ContractedDataset dataset = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("a", TypeContractorFactory.STRING)
            .addContractor("b", TypeContractorFactory.STRING)
            .build()
        );
        
        dataset.addDataObject(new DataObject()
            .set("a", "8")
            .set("b", "9"));
        dataset.addDataObject(new DataObject()
            .set("a", "9")
            .set("b", "10"));
        
        ContractedDataset expected = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("a", TypeContractorFactory.INTEGER)
            .addContractor("b", TypeContractorFactory.INTEGER)
            .build()
        );
        
        expected.addDataObject(new DataObject()
            .set("a", 8)
            .set("b", 9));
        expected.addDataObject(new DataObject()
            .set("a", 9)
            .set("b", 10));
        
        ContractedDataset casted = dataset.asInteger(
            SetOperations.set("a","b"),
            TypeContractorFactory.INTEGER
        );
        
        assertEquals(expected, casted);
    }
}
