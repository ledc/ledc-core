package be.ugent.ledc.core.test;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import java.util.Comparator;
import java.util.HashSet;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Contains a series of unit tests to verify the functionality of SimpleDataset operations like selection, head, tail...
 * @author abronsel
 */
public class SimpleDatasetTest
{
    @Test
    public void sizeTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "xxx"));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "xxy"));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "xzx"));
        
        assertEquals(3, dataset.getSize());
    }
    
    @Test
    public void headTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "xxx"));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "xxy"));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "xzx"));
        
        SimpleDataset head = new SimpleDataset();
        
        head.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "xxx"));
        
        assertEquals(head, dataset.head(1));
        assertEquals(new SimpleDataset(), dataset.head(-2));
        assertEquals(dataset, dataset.head(5));
    }
    
    @Test
    public void tailTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "xxx"));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "xxy"));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "xzx"));
        
        SimpleDataset tail = new SimpleDataset();
        
        tail.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "xxy"));
        tail.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "xzx"));
        
        assertEquals(tail, dataset.tail(2));
        assertEquals(new SimpleDataset(), dataset.tail(-2));
        assertEquals(dataset, dataset.tail(5));
    }
    
    @Test
    public void projectionTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "xxx"));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "xxy"));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "xzx"));
        
        SimpleDataset projection = new SimpleDataset();
        
        projection.addDataObject(new DataObject().set("a", 1));
        projection.addDataObject(new DataObject().set("a", 2));
        projection.addDataObject(new DataObject().set("a", 3));
        
        SimpleDataset emptyProjection = new SimpleDataset();
        emptyProjection.addDataObject(new DataObject());
        emptyProjection.addDataObject(new DataObject());
        emptyProjection.addDataObject(new DataObject());
        
        
        assertEquals(projection, dataset.project("a"));
        assertEquals(emptyProjection, dataset.project());

    }
    
    @Test
    public void selectionWithIntegerTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "xxx"));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "xxy"));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "xzx"));
        dataset.addDataObject(new DataObject()
            .set("a", 4)
            .set("b", "xzx"));
        
        SimpleDataset selection = new SimpleDataset();
        
        selection.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "xxx"));
        selection.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "xxy"));
        
        
        assertEquals(selection, dataset.select(o -> o.getInteger("a") <= 2));

    }
    
    @Test
    public void selectionWithRegexTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "xxx"));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "xxy"));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "xzx"));
        dataset.addDataObject(new DataObject()
            .set("a", 4)
            .set("b", "xzx"));
        
        SimpleDataset selection = new SimpleDataset();
        
        selection.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "xzx"));
        selection.addDataObject(new DataObject()
            .set("a", 4)
            .set("b", "xzx"));
        
        
        assertEquals(selection, dataset.select(o -> o.getString("b").matches(".+z.+")));

    }
    
    @Test
    public void sortTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 8)
            .set("b", "third"));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "first"));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "second"));
        dataset.addDataObject(new DataObject()
            .set("a", 10)
            .set("b", "fourth"));
        
        Dataset sorted = dataset.sort(Comparator.comparing((o) -> o.getInteger("a")));

        assertEquals(4, sorted.getSize());
        assertEquals("first", sorted.getDataObjects().get(0).getString("b"));
        assertEquals("second", sorted.getDataObjects().get(1).getString("b"));
        assertEquals("third", sorted.getDataObjects().get(2).getString("b"));
        assertEquals("fourth", sorted.getDataObjects().get(3).getString("b"));
    }
    
    @Test
    public void sortObjectComparatorTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 8)
            .set("b", "third"));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "first"));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "second"));
        dataset.addDataObject(new DataObject()
            .set("a", 10)
            .set("b", "fourth"));
        
        Dataset sorted = dataset.sort(DataObject.getProjectionComparator("a"));

        assertEquals(4, sorted.getSize());
        assertEquals("first", sorted.getDataObjects().get(0).getString("b"));
        assertEquals("second", sorted.getDataObjects().get(1).getString("b"));
        assertEquals("third", sorted.getDataObjects().get(2).getString("b"));
        assertEquals("fourth", sorted.getDataObjects().get(3).getString("b"));
    }
    
    @Test
    public void sortWithNullsTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", null));
        dataset.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", 1));
        dataset.addDataObject(new DataObject()
            .set("a", null)
            .set("b", 2));
        dataset.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 2));
        dataset.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", 3));
        dataset.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 1));
        
        Dataset sorted = dataset.sort(DataObject.getProjectionComparator("a","b"));

        SimpleDataset expected = new SimpleDataset();
        expected.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 1));
        expected.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 2));
        expected.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", 3));
        expected.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", null));
        expected.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", 1));
        expected.addDataObject(new DataObject()
            .set("a", null)
            .set("b", 2));

        assertEquals(expected, sorted);
    }
    
    @Test
    public void distinctTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", 8)
            .set("b", "xx"));
        dataset.addDataObject(new DataObject()
            .set("a", 9)
            .set("b", "xx"));
        dataset.addDataObject(new DataObject()
            .set("a", 8)
            .set("b", "xx"));
        dataset.addDataObject(new DataObject()
            .set("a", 9)
            .set("b", "xx"));
        dataset.addDataObject(new DataObject()
            .set("a", 9)
            .set("b", "xxy"));
        
        Dataset distinct = dataset.distinct();

        assertEquals(3, distinct.getSize());
        assertEquals(new HashSet<>(dataset.getDataObjects()), new HashSet<>(distinct.getDataObjects()));
    }
    
    @Test
    public void castToIntTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", "8")
            .set("b", "xx"));
        dataset.addDataObject(new DataObject()
            .set("a", "9")
            .set("b", "xx"));
        
        SimpleDataset expected = new SimpleDataset();
        
        expected.addDataObject(new DataObject()
            .set("a", 8)
            .set("b", "xx"));
        expected.addDataObject(new DataObject()
            .set("a", 9)
            .set("b", "xx"));
        
        assertEquals(expected, dataset.asInteger("a"));
        
    }
    
    @Test
    public void groupByCountTest()
    {
        SimpleDataset dataset = new SimpleDataset();
        
        dataset.addDataObject(new DataObject()
            .set("a", "8")
            .set("b", "xx"));
        
        dataset.addDataObject(new DataObject()
            .set("a", "9")
            .set("b", "xx"));
        
        dataset.addDataObject(new DataObject()
            .set("a", "9")
            .set("b", "xx"));
        
        dataset.addDataObject(new DataObject()
            .set("a", "9")
            .set("b", "xx"));
        
        SimpleDataset expected = new SimpleDataset();
        
        expected.addDataObject(
            new DataObject()
                .setString("a", "8")
                .setInteger("count", 1)
        );
        
        expected.addDataObject(
            new DataObject()
                .setString("a", "9")
                .setInteger("count", 3)
        );
        
        assertEquals(expected, dataset
            .groupByAndCount("count", "a")
            .sort(DataObject.getProjectionComparator("count")));
        
    }
}
