package be.ugent.ledc.core.test;

import be.ugent.ledc.core.binding.csv.CSVLineParser;
import be.ugent.ledc.core.binding.csv.CSVProperties;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.util.SetOperations;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Some tests to check if CSVLines are correctly parsed into objects
 * @author abronsel
 */
public class CSVTest
{
    @Test
    public void basicLineTest()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", null);
        
        String line = "a;b;c";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", "c");
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void noQuotesTest()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", null);
        
        String line = "a;b;\"c\"";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", "\"c\"");
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void quotesTest()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", '"');
        
        String line = "a;b;\"c\"";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", "c");
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void quotedSeparatorTest()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", '"');
        
        String line = "a;b;\"c;d\"";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", "c;d");
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void quotedQouteTest()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", '"');
        
        String line = "a;b;\"I'm almost;\" there...";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", "I'm almost; there...");
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void quotedQouteTest2()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", '"');
        
        String line = "\"xx\";\"Omnibus \"\"The Second Mystery Book\"\"; 1945\"";
        
        DataObject expected = new DataObject()
            .setString("0", "xx")
            .setString("1", "Omnibus \"The Second Mystery Book\"; 1945");
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void nullTest1()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", '"');
        
        String line = "a;b;";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", null);
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void nullTest2()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", '"');
        
        String line = "a;b;null";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", "null");
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void nullTest3()
    {
        CSVProperties csvProps = new CSVProperties(true, ";", '"', SetOperations.set("","null"));
        
        String line = "a;;null";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", null)
            .setString("2", null);
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void tabularTest()
    {
        CSVProperties csvProps = new CSVProperties(true, "\t", '"', SetOperations.set("","null"));
        
        String line = "a\tb\tc";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", "c");
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
    
    @Test
    public void tabularWithTrailingNullTest()
    {
        CSVProperties csvProps = new CSVProperties(true, "\t", '"', SetOperations.set("","null"));
        
        String line = "a\tb\t";
        
        DataObject expected = new DataObject()
            .setString("0", "a")
            .setString("1", "b")
            .setString("2", null);
        
        assertEquals(expected, CSVLineParser.parseLine(line, csvProps));
    }
}
