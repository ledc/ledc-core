package be.ugent.ledc.core.test;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.datastructures.kdtree.BoundingBox;
import be.ugent.ledc.core.datastructures.kdtree.KDTree;
import be.ugent.ledc.core.datastructures.kdtree.KDTreeFactory;
import be.ugent.ledc.core.operators.metric.DataObjectMetrics;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author abronsel
 */
public class KDTreeTest
{
    private static ContractedDataset dataset;

    @BeforeClass
    public static void init()
    {
        dataset = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("x", TypeContractorFactory.INTEGER)
            .addContractor("y", TypeContractorFactory.INTEGER)
            .build()
        );

        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 1)
                    .setInteger("y", 3)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 1)
                    .setInteger("y", 8)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 2)
                    .setInteger("y", 2)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 2)
                    .setInteger("y", 10)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 3)
                    .setInteger("y", 6)
            );
        
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 4)
                    .setInteger("y", 1)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 5)
                    .setInteger("y", 4)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 6)
                    .setInteger("y", 8)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 7)
                    .setInteger("y", 4)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 7)
                    .setInteger("y", 7)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 8)
                    .setInteger("y", 2)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 8)
                    .setInteger("y", 5)
            );
        dataset
            .addDataObject(
                new DataObject()
                    .setInteger("x", 9)
                    .setInteger("y", 9)
            );
        
        
        
    }

    @Test
    public void creationTest()
    {
        KDTree<Integer> kdTree = KDTreeFactory.create(dataset, TypeContractorFactory.INTEGER);
        
        DataObject expectedRoot = new DataObject()
            .setInteger("x", 5)
            .setInteger("y", 4);
        
        Assert.assertEquals(expectedRoot, kdTree.getRoot().getPoint());
    }
    
    @Test
    public void boundingBoxTest()
    {
        BoundingBox<Integer> b = new BoundingBox<>(dataset.getContract().getAttributes());
        
        BoundingBox updated = b
        .leftUpdate("x", 5)
        .rightUpdate("y", 6)
        .rightUpdate("x", 2);
        
        assertTrue(updated
            .contains(
                new DataObject()
                    .setInteger("x", 4)
                    .setInteger("y", 8),
                TypeContractorFactory.INTEGER));
    }
    
    @Test
    public void nearestNeighborTest()
    {
        DataObject query = new DataObject()
            .setInteger("x", 4)
            .setInteger("y", 8);
        
        DataObject result = KDTreeFactory
            .create(dataset, TypeContractorFactory.INTEGER)
            .nearest(query, DataObjectMetrics.EUCLIDEAN);
        
        DataObject expected = new DataObject()
            .setInteger("x", 6)
            .setInteger("y", 8);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void radialSearchTest1()
    {
        DataObject query = new DataObject()
            .setInteger("x", 4)
            .setInteger("y", 8);
        
        List<DataObject> neighborhood = KDTreeFactory
            .create(dataset, TypeContractorFactory.INTEGER)
            .radialSearch(query, 1.0, DataObjectMetrics.EUCLIDEAN);
        
        assertTrue(neighborhood.isEmpty());
    }
    
    @Test
    public void radialSearchTest2()
    {
        DataObject query = new DataObject()
            .setInteger("x", 4)
            .setInteger("y", 8);
        
        List<DataObject> neighborhood = KDTreeFactory
            .create(dataset, TypeContractorFactory.INTEGER)
            .radialSearch(query, 2.5, DataObjectMetrics.EUCLIDEAN);
        
        assertEquals(2, neighborhood.size());
        assertTrue(neighborhood.contains(
            new DataObject()
            .setInteger("x", 6)
            .setInteger("y", 8)));
        assertTrue(neighborhood.contains(
            new DataObject()
            .setInteger("x", 3)
            .setInteger("y", 6)));
    }
}
