package be.ugent.ledc.core.test;

import be.ugent.ledc.core.operators.UnitScore;
import be.ugent.ledc.core.operators.aggregation.OwaAggregator;
import be.ugent.ledc.core.operators.quantifier.BasicQuantifier;
import org.junit.Assert;
import org.junit.Test;

/**
 * Some tests to check if quantified aggregation is executed properly
 * @author abronsel
 */
public class QuantifiedAggregationTest
{
    
    @Test
    public void owaAverageTest()
    {
        UnitScore result = new OwaAggregator()
            .aggregate(
                new UnitScore(1.0),
                new UnitScore(0.0),
                new UnitScore(0.5),
                new UnitScore(0.5)
            );
        
        Assert.assertEquals(0.5,result.getValue(), 0.0001);
    }
    
    @Test
    public void owaAnyTest()
    {
        UnitScore result = new OwaAggregator(BasicQuantifier.ANY)
            .aggregate(
                new UnitScore(1.0),
                new UnitScore(0.0),
                new UnitScore(0.5),
                new UnitScore(0.5)
            );
        
        Assert.assertEquals(1.0,result.getValue(), 0.0001);
    }
    
    @Test
    public void owaAllTest()
    {
        UnitScore result = new OwaAggregator(BasicQuantifier.ALL)
            .aggregate(
                new UnitScore(1.0),
                new UnitScore(0.0),
                new UnitScore(0.5),
                new UnitScore(0.5)
            );
        
        Assert.assertEquals(0.0,result.getValue(), 0.0001);
    }
    
    @Test
    public void owaSomeTest()
    {
        UnitScore result = new OwaAggregator(BasicQuantifier.SOME)
            .aggregate(
                new UnitScore(1.0),
                new UnitScore(0.0),
                new UnitScore(0.5),
                new UnitScore(0.5)
            );
        
        Assert.assertEquals(    
            0.68301270189221932338186158537647,
            result.getValue(),
            0.0001
        );
    }
}
