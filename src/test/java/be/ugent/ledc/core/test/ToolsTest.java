package be.ugent.ledc.core.test;

import be.ugent.ledc.core.operators.string.NGramSplitter;
import be.ugent.ledc.core.operators.string.WordSplitter;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import java.util.Set;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author abronsel
 */
public class ToolsTest
{
    @Test
    public void intersectionTest()
    {
        Set<String> a = SetOperations.set("a", "b", "c");
        Set<String> b = SetOperations.set("a", "c", "d");
       
        Set<String> expected = SetOperations.set("a", "c");
        
        Assert.assertEquals(expected, SetOperations.intersect(a, b));
    }
    
    @Test
    public void stringSplitNGramTest()
    {
        String s = "hey there";
        
        assertEquals(
            ListOperations.list(
                "hey there"
            ),
            new NGramSplitter(20).split(s));
        
        assertEquals(
            ListOperations.list(
                "h",
                "e",
                "y",
                " ",
                "t",
                "h",
                "e",
                "r",
                "e"
            ),
            new NGramSplitter(0).split(s));
        
        assertEquals(
            ListOperations.list(
                "hey",
                "ey ",
                "y t",
                " th",
                "the",
                "her",
                "ere"
            ),
            new NGramSplitter(3).split(s));
    }
    
    @Test
    public void wordSplitTest()
    {
        String s = "hey there";
        String t = "hey there   how are     you   ";
        
        assertEquals(
            ListOperations.list(
                "hey",
                "there"
            ),
            new WordSplitter().split(s));
        
        assertEquals(
            ListOperations.list(
                "hey",
                "there",
                "how",
                "are",
                "you"
            ),
            new WordSplitter().split(t));

    }
    
}
