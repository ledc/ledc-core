package be.ugent.ledc.core.test;

import be.ugent.ledc.core.operators.UnitScore;
import be.ugent.ledc.core.operators.aggregation.tnorm.BasicTNorm;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Contains a series of tests to verify the functionality of aggregation operators (TNorms, means...)
 * @author abronsel
 */
public class TNormTests
{
    @Test
    public void minimumTNormTest()
    {
        UnitScore a = new UnitScore(0.7);
        UnitScore b = new UnitScore(0.5);
        
        UnitScore c = BasicTNorm.MINIMUM.aggregate(a,b);
        
        assertEquals(0.5, c.getValue(), 0.0000001);
    }
    
    @Test
    public void productTNormTest()
    {
        UnitScore a = new UnitScore(0.7);
        UnitScore b = new UnitScore(0.5);
        
        UnitScore c = BasicTNorm.PRODUCT.aggregate(a,b);
        
        assertEquals(0.35, c.getValue(), 0.0000001);
    }
    
    @Test
    public void LukasiewicsTNormTest()
    {
        UnitScore a = new UnitScore(0.7);
        UnitScore b = new UnitScore(0.5);
        
        UnitScore c = BasicTNorm.LUKASIEWICS.aggregate(a,b);
        
        assertEquals(0.2, c.getValue(), 0.0000001);
    }
    
    @Test
    public void HamacherProductTNormTest()
    {
        UnitScore a = new UnitScore(0.7);
        UnitScore b = new UnitScore(0.5);
        
        UnitScore c = BasicTNorm.HAMACHER_PRODUCT.aggregate(a,b);
        
        assertEquals(0.41176470588235294117647058823529, c.getValue(), 0.0000001);
    }
    
    @Test
    public void HamacherProductTNormTestEdgeCase()
    {
        UnitScore a = UnitScore.ZERO;
        UnitScore b = UnitScore.ZERO;
        
        UnitScore c = BasicTNorm.HAMACHER_PRODUCT.aggregate(a,b);
        
        assertEquals(0, c.getValue(), 0.0000001);
    }
    
    @Test
    public void DrasticTNormTest()
    {
        UnitScore a = new UnitScore(0.7);
        UnitScore b = new UnitScore(0.5);
        
        UnitScore c = BasicTNorm.DRASTIC.aggregate(a,b);
        
        assertEquals(0.0, c.getValue(), 0.0000001);
    }
    
    @Test
    public void DrasticTNormTestEdgeCase()
    {
        UnitScore a = UnitScore.ONE;
        UnitScore b = new UnitScore(0.5);
        
        UnitScore c = BasicTNorm.DRASTIC.aggregate(a,b);
        
        assertEquals(0.5, c.getValue(), 0.0000001);
    }
}
