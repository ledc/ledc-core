package be.ugent.ledc.core.test;

import be.ugent.ledc.core.DataException;
import be.ugent.ledc.core.dataset.DataObject;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Contains a series of unit tests to verify the functionality of DataObject operations
 * @author abronsel
 */
public class DataObjectTest
{
    @Rule
    public ExpectedException dataExceptionRule = ExpectedException.none();
    
    @Test
    public void creationLowerCaseTest()
    {
        DataObject o = new DataObject();
        
        o.set("CasingIgnored", 4);
        o.set("hello", 3);
        
        Set<String> attributes = Stream.of("casingignored", "hello").collect(Collectors.toSet());
        
        assertEquals(attributes, o.getAttributes());
    }
    
    @Test
    public void creationWithCasingTest()
    {
        DataObject o = new DataObject(true);
        
        o.set("CasingIgnored", 4);
        o.set("hello", 3);
        
        Set<String> attributes = Stream.of("CasingIgnored", "hello").collect(Collectors.toSet());
        
        assertEquals(attributes, o.getAttributes());
    }
    
    @Test
    public void getWithIntegerTypeTest()
    {
        DataObject o = new DataObject();
        
        o.set("a", 4);
        
        assertEquals(4, o.getInteger("a").intValue());
    }
    
    @Test
    public void getWithStringTypeTest()
    {
        DataObject o = new DataObject();
        
        o.set("a", "4");
        
        assertEquals("4", o.getString("a"));
    }
    
    @Test
    public void getWithListTypeTest()
    {
        DataObject o = new DataObject();
        
        List list = Stream.of("1", "2", "3").collect(Collectors.toList());
        
        o.setList("a", list);
        
        assertEquals(list, o.getList("a"));
    }
    
    @Test
    public void getWithDataObjectTypeTest()
    {
        DataObject nested = new DataObject().set("a", 1);
        
        DataObject o = new DataObject()
            .setDataObject("nested", nested);
        
        assertEquals(nested, o.getDataObject("nested"));
    }
    
    @Test
    public void getStringTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", 4);
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type String.");
        o.getString("a");
    }
    
    @Test
    public void getLongTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", 4);
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type Long.");
        o.getLong("a");
    }
    
    @Test
    public void getIntegerTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", "4");
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type Integer.");
        o.getInteger("a");
    }
    
    @Test
    public void getBooleanTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", "4");
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type Boolean.");
        o.getBoolean("a");
    }
    
    @Test
    public void getBigDecimalTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", "4");
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type BigDecimal.");
        o.getBigDecimal("a");
    }
    
    @Test
    public void getFloatTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", "4");
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type Float.");
        o.getFloat("a");
    }
    
    @Test
    public void getDoubleTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", "4");
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type Double.");
        o.getDouble("a");
    }
    
    @Test
    public void getDateTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", "4");
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type LocalDate.");
        o.getDate("a");
    }
    
    @Test
    public void getTimeTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", "4");
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type LocalTime.");
        o.getTime("a");
    }
    
    @Test
    public void getDatetimeTypeWrongTest()
    {
        DataObject o = new DataObject()
            .set("a", "4");
        
        dataExceptionRule.expect(DataException.class);
        dataExceptionRule.expectMessage("Attribute 'a' is not of type LocalDateTime.");
        o.getDateTime("a");
    }
    
    @Test
    public void projectionTest()
    {
        DataObject o = new DataObject()
            .set("a", 1)
            .set("b", "x")
            .set("c", 2);
        
        DataObject projection = new DataObject()
            .set("a", 1)
            .set("b", "x");
        
        assertEquals(projection, o.project("a", "b"));
    }
    
    @Test
    public void inverseProjectionTest()
    {
        DataObject o = new DataObject()
            .set("a", 1)
            .set("b", "x")
            .set("c", 2);
        
        DataObject inverseProjection = new DataObject()
            .set("c", 2);
        
        assertEquals(inverseProjection, o.inverseProject("a", "b"));
    }
    
    @Test
    public void isProjectionTest()
    {
        DataObject o = new DataObject()
            .set("a", 1)
            .set("b", "x");
        
        DataObject superObject = new DataObject()
            .set("a", 1)
            .set("b", "x")
            .set("c", "y");
        
        DataObject noSuperObject = new DataObject()
            .set("a", 1)
            .set("c", "y");
        
        assertEquals(true, o.isProjectionOf(superObject));
        assertEquals(true, o.isProjectionOf(o));
        assertEquals(false, o.isProjectionOf(noSuperObject));
    }
    
    @Test
    public void concatTest()
    {
        DataObject o1 = new DataObject()
            .set("a", 1)
            .set("b", "x");
        
        DataObject o2 = new DataObject(true)
            .set("B", "y")
            .set("C", 2);
        
        DataObject concat = new DataObject()
            .set("a", 1)
            .set("b", "y")
            .set("c", 2);
        
        assertEquals(concat, o1.concat(o2));
    }
    
    
    @Test
    public void subsumptionTest()
    {
        DataObject o1 = new DataObject()
            .set("a", 1)
            .set("b", null)
            .set("c", 3);
        
        DataObject o2 = new DataObject()
            .set("C", 3);
        
        DataObject o3 = new DataObject()
            .set("B", 1)
            .set("C", 3);

        assertTrue(o1.subsumes(o2));
        assertFalse(o1.subsumes(o3));
    }
    
    @Test
    public void complementationTest()
    {
        DataObject o1 = new DataObject()
            .set("a", 1)
            .set("b", null)
            .set("c", 3);
        
        DataObject o2 = new DataObject()
            .set("a", 1)
            .set("b", 1)
            .set("d", 4);

        assertTrue(o1.complements(o2));
        assertTrue(o2.complements(o1));
    }
    
    @Test
    public void conversionStringToIntegerTest()
    {
        DataObject in = new DataObject().set("a", "1");

        DataObject expected = new DataObject().set("a", 1);

        assertEquals(expected, in.asInteger("a"));
    }
    
    @Test
    public void conversionStringToLongTest()
    {
        DataObject in = new DataObject().set("a", "1");

        DataObject expected = new DataObject().set("a", 1L);

        assertEquals(expected, in.asLong("a"));
    }
    
    @Test
    public void conversionDoubleToIntegerTest()
    {
        DataObject in = new DataObject().set("a", 1.15);
        DataObject in2 = new DataObject().set("a", 1.65);

        DataObject expected = new DataObject().set("a", 1);
        DataObject expected2 = new DataObject().set("a", 2);

        assertEquals(expected, in.asInteger("a"));
        assertEquals(expected2, in2.asInteger("a"));
    }
    
    @Test
    public void conversionStringToDateTimeTest()
    {
        DataObject in = new DataObject().set("a", "2007-12-03T10:15:30");

        DataObject expected = new DataObject().set("a", LocalDateTime.of(2007, Month.DECEMBER, 3, 10, 15, 30));

        assertEquals(expected, in.asDateTime("a"));
    }
    
    @Test
    public void conversionStringToDateTimeWithPatternTest()
    {
        DataObject in = new DataObject().set("a", "2007/12/03 10:15");

        DataObject expected = new DataObject().set("a", LocalDateTime.of(2007, Month.DECEMBER, 3, 10, 15));

        assertEquals(expected, in.asDateTime("a", "yyyy/MM/dd HH:mm"));
    }
    
    @Test
    public void conversionStringToDateTimeWithTimezoneTest()
    {
        DataObject in = new DataObject().set("a", "2007/12/03 10:15:00+00");

        DataObject expected = new DataObject().set("a", LocalDateTime.of(2007, Month.DECEMBER, 3, 10, 15));

        assertEquals(expected, in.asDateTime("a", "yyyy/MM/dd HH:mm:ssX"));
    }
    
    @Test
    public void diffTest()
    {
        DataObject o1 = new DataObject()
            .set("a", "1")
            .set("b", "2")
            .set("c", "3");
        
        DataObject o2 = new DataObject()
            .set("a", "1")
            .set("b", 2)
            .set("d", "3");
        
        Set<String> expected = Stream.of("b", "c", "d").collect(Collectors.toSet());
        
        assertEquals(expected, o1.diff(o2));
    }
    
    @Test
    public void diffTestWithNull()
    {
        DataObject o1 = new DataObject()
            .set("a", "1")
            .set("b", null)
            .set("c", "3");
        
        DataObject o2 = new DataObject()
            .set("a", "1")
            .set("b", 2)
            .set("d", "3");
        
        Set<String> expected = Stream.of("b", "c", "d").collect(Collectors.toSet());
        
        assertEquals(expected, o1.diff(o2));
    }
    
    @Test
    public void deriveTest()
    {
        DataObject o = new DataObject()
            .setInteger("a", 6)
            .setInteger("b", 5);
        
        DataObject expected = new DataObject()
            .setInteger("a", 6)
            .setInteger("b", 5)
            .setInteger("c", 11);
        
        assertEquals(expected, o.extend("c", r -> r.getInteger("a") + r.getInteger("b")));
    }
    
    @Test
    public void hashCodeTest()
    {
        assertTrue(
            new DataObject().set("a", 1).set("b", 500).hashCode() ==
            new DataObject().set("b", 500).set("a", 1).hashCode()
        );
    }
    
    @Test
    public void hashCodeTest2()
    {
        DataObject o = new DataObject().set("a", 1).set("b", 500);
        assertTrue(o.hashCode() == o.hashCode());
    }
}
