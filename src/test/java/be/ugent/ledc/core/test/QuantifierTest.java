package be.ugent.ledc.core.test;

import be.ugent.ledc.core.operators.UnitScore;
import be.ugent.ledc.core.operators.quantifier.AtLeastQuantifier;
import be.ugent.ledc.core.operators.quantifier.BasicQuantifier;
import be.ugent.ledc.core.operators.quantifier.Quantifier;
import be.ugent.ledc.core.operators.quantifier.QuantifierFactory;
import org.junit.Assert;
import org.junit.Test;

public class QuantifierTest
{
    @Test
    /**
     * Verifies that all BasicQuantifiers satisfy the normalization condition
     */
    public void basicQuantifierNormalizationTest()
    {
        for(BasicQuantifier q: BasicQuantifier.values())
        {
            Assert.assertTrue(
                UnitScore.isOne(q.apply(UnitScore.ONE))
            );
        }
    }
    
    @Test
    /**
     * Verifies monotonicity of basic quantifiers
     */
    public void basicQuantifierInternalMonotonicityTest()
    {
        for(BasicQuantifier q: BasicQuantifier.values())
        {
            for(int i=0; i<100; i++)
            {
                UnitScore u1 = q.apply(UnitScore.fromFraction(i, 100));
                UnitScore u2 = q.apply(UnitScore.fromFraction(i+1, 100));
                
                Assert.assertTrue(u1.compareTo(u2) <= 0);
            }
        }
    }
    
    @Test
    /**
     * Verifies mutual monotonicity of basic quantifiers in-line with their
     * linguistic descriptions. That is a strong constraint on the quantity should
     * result in a lower degree of correspondence.
     */
    public void basicQuantifierMutualMonotonicityTest()
    {
        BasicQuantifier[] order = new BasicQuantifier[]
        {
            BasicQuantifier.ALL,
            BasicQuantifier.MOST,
            BasicQuantifier.MANY,
            BasicQuantifier.AVERAGE,
            BasicQuantifier.SOME,
            BasicQuantifier.FEW,
            BasicQuantifier.ANY
        };

        for(int i=0; i<100; i++)
        {
            for(int j=1;j<order.length; j++)
            {
                UnitScore higher = order[j].apply(UnitScore.fromFraction(i, 100));
                UnitScore lower = order[j-1].apply(UnitScore.fromFraction(i, 100));
                
                Assert.assertTrue(lower.compareTo(higher) <= 0);
            }            
        }
    }
    
    @Test
    public void conjunctionTest1()
    {
        Quantifier q1 = BasicQuantifier.ALL;
        Quantifier q2 = BasicQuantifier.FEW;
        
        Quantifier composed = QuantifierFactory.and(q1,q2);
        
        for(int i=0; i<100; i++)
        {
            Assert
                .assertEquals(
                    q1.apply(UnitScore.fromFraction(i, 100)).getValue(),
                    composed.apply(UnitScore.fromFraction(i, 100)).getValue(),
                    Double.MIN_VALUE
                );
        }
    }
    
    @Test
    public void conjunctionTest2()
    {
        Quantifier q1 = new AtLeastQuantifier(new UnitScore(0.6));
        Quantifier q2 = BasicQuantifier.FEW;
        
        Quantifier composed = QuantifierFactory.and(q1,q2);

        Assert.assertEquals(
            UnitScore.ZERO.getValue(),
            composed.apply(new UnitScore(0.5)).getValue(),
            Double.MIN_VALUE
        );
        
        Assert.assertEquals(
            q2.apply(new UnitScore(0.7)).getValue(),
            composed.apply(new UnitScore(0.7)).getValue(),
            Double.MIN_VALUE
        );
    }
    
    @Test
    public void disjunctionTest2()
    {
        Quantifier q1 = new AtLeastQuantifier(new UnitScore(0.6));
        Quantifier q2 = BasicQuantifier.FEW;
        
        Quantifier composed = QuantifierFactory.or(q1,q2);

        Assert.assertEquals(
            q2.apply(new UnitScore(0.5)).getValue(),
            composed.apply(new UnitScore(0.5)).getValue(),
            Double.MIN_VALUE
        );
        
        Assert.assertEquals(
            UnitScore.ONE.getValue(),
            composed.apply(new UnitScore(0.7)).getValue(),            
            Double.MIN_VALUE
        );
    }
    
    @Test
    public void shiftLeftNoSqueezeTest()
    {
        Quantifier q = BasicQuantifier.FEW;
        
        Quantifier modified = QuantifierFactory.shiftLeft(q,new UnitScore(0.5), false);

        Assert.assertTrue(UnitScore.isZero(modified.apply(UnitScore.ZERO)));
        
        Assert.assertEquals(
            UnitScore.ONE.getValue(),
            modified.apply(new UnitScore(0.5)).getValue(),
            Double.MIN_VALUE
        );
        
        Assert.assertEquals(
            q.apply(new UnitScore(0.8)).getValue(),
            modified.apply(new UnitScore(0.3)).getValue(),
            Double.MIN_VALUE
        );
    }
    
    @Test
    public void shiftLeftSqueezeTest()
    {
        Quantifier q = BasicQuantifier.FEW;
        
        Quantifier modified = QuantifierFactory.shiftLeft(q,new UnitScore(0.5), true);

        Assert.assertTrue(UnitScore.isZero(modified.apply(UnitScore.ZERO)));
        
        Assert.assertEquals(
            UnitScore.ONE.getValue(),
            modified.apply(new UnitScore(0.5)).getValue(),
            Double.MIN_VALUE
        );
        
        Assert.assertEquals(
            q.apply(new UnitScore(0.6)).getValue(),
            modified.apply(new UnitScore(0.3)).getValue(),
            Double.MIN_VALUE
        );
    }
    
    @Test
    public void shiftRightNoSqueezeTest()
    {
        Quantifier q = BasicQuantifier.FEW;
        
        Quantifier modified = QuantifierFactory.shiftRight(q,new UnitScore(0.5), false);

        Assert.assertTrue(UnitScore.isOne(modified.apply(UnitScore.ONE)));
        
        Assert.assertEquals(
            UnitScore.ZERO.getValue(),
            modified.apply(new UnitScore(0.5)).getValue(),
            Double.MIN_VALUE
        );
        
        Assert.assertEquals(
            q.apply(new UnitScore(0.3)).getValue(),
            modified.apply(new UnitScore(0.8)).getValue(),
            Double.MIN_VALUE
        );
    }
    
    @Test
    public void shiftRightSqueezeTest()
    {
        Quantifier q = BasicQuantifier.FEW;
        
        Quantifier modified = QuantifierFactory.shiftRight(q,new UnitScore(0.5), true);

        Assert.assertTrue(UnitScore.isOne(modified.apply(UnitScore.ONE)));
        
         Assert.assertEquals(
            UnitScore.ZERO.getValue(),
            modified.apply(new UnitScore(0.5)).getValue(),
            Double.MIN_VALUE
        );
        
        Assert.assertEquals(
            q.apply(new UnitScore(0.2)).getValue(),
            modified.apply(new UnitScore(0.6)).getValue(),
            Double.MIN_VALUE
        );
    }
    
}
