package be.ugent.ledc.core.test;

import be.ugent.ledc.core.datastructures.Multiset;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Contains a series of unit tests to verify the functionality of multiset operations like intersection, union, difference...
 * @author abronsel
 */
public class MultisetTest
{
    @Test
    public void multiplicityTest()
    {
        Multiset<String> multiset = new Multiset(Stream.of("a", "a", "a", "b", "b").collect(Collectors.toList()));
        
        assertEquals(3, multiset.multiplicity("a").intValue());
        assertEquals(2, multiset.multiplicity("b").intValue());
        assertEquals(0, multiset.multiplicity("c").intValue());
    }
    
    @Test
    public void cardinalityTest()
    {
        Multiset<String> multiset = new Multiset(Stream.of("a", "a", "a", "b", "b").collect(Collectors.toList()));
        
        assertEquals(5, multiset.cardinality());
    }
    
    @Test
    public void maximumTest()
    {
        Multiset<String> multiset = new Multiset(Stream.of("a", "a", "a", "b", "b").collect(Collectors.toList()));
        
        assertEquals(3, multiset.maximum());
    }
    
    @Test
    public void minimumTest()
    {
        Multiset<String> multiset = new Multiset(Stream.of("a", "a", "a", "b", "b").collect(Collectors.toList()));
        
        assertEquals(2, multiset.minimum());
    }

    @Test
    public void elementAdditionTest()
    {
        Multiset<String> multiset = new Multiset(Stream.of("a", "a").collect(Collectors.toList()));
        Multiset<String> added = new Multiset(Stream.of("a", "a", "a", "a", "a").collect(Collectors.toList()));
        
        multiset.add("a", 3);
        
        assertEquals(added, multiset);
    }
    
    @Test
    public void sumTest()
    {
        Multiset<String> multiset1 = new Multiset(Stream.of("a", "a", "b", "b").collect(Collectors.toList()));
        Multiset<String> multiset2 = new Multiset(Stream.of("a", "c", "c").collect(Collectors.toList()));
        
        Multiset<String> sum = new Multiset(Stream.of("a", "a", "a", "b", "b", "c", "c").collect(Collectors.toList()));
        
        assertEquals(sum, multiset1.sum(multiset2));
    }
    
    @Test
    public void minusTest()
    {
        Multiset<String> multiset1 = new Multiset(Stream.of("a", "a", "b", "c").collect(Collectors.toList()));
        Multiset<String> multiset2 = new Multiset(Stream.of("a", "b", "b").collect(Collectors.toList()));
        
        Multiset<String> difference = new Multiset(Stream.of("a", "c").collect(Collectors.toList()));
        
        assertEquals(difference, multiset1.minus(multiset2));
        assertTrue(!multiset1.minus(multiset2).containsKey("b"));
    }
    
    @Test
    public void intersectionTest()
    {
        Multiset<String> multiset1 = new Multiset(Stream.of("a", "a", "b", "c").collect(Collectors.toList()));
        Multiset<String> multiset2 = new Multiset(Stream.of("a", "b", "b").collect(Collectors.toList()));
        
        Multiset<String> intersection = new Multiset(Stream.of("a", "b").collect(Collectors.toList()));
        
        assertEquals(intersection, multiset1.intersection(multiset2));
    }
    
    @Test
    public void unionTest()
    {
        Multiset<String> multiset1 = new Multiset(Stream.of("a", "a", "b", "c").collect(Collectors.toList()));
        Multiset<String> multiset2 = new Multiset(Stream.of("a", "b", "b").collect(Collectors.toList()));
        
        Multiset<String> union = new Multiset(Stream.of("a", "a", "b", "b", "c").collect(Collectors.toList()));
        
        assertEquals(union, multiset1.union(multiset2));
    }
    
    @Test
    public void cutoffTest()
    {
        Multiset<String> multiset = new Multiset(Stream.of("a", "a", "a", "b", "b", "c").collect(Collectors.toList()));
        
        Multiset<String> cutoff = new Multiset(Stream.of("a", "b").collect(Collectors.toList()));
        
        assertEquals(cutoff, multiset.multiplicityCut(2));
    }
    
    @Test
    public void trimTest()
    {
        Multiset<String> multiset = new Multiset(Stream.of("a", "a", "a", "b", "b", "c").collect(Collectors.toList()));
        
        Multiset<String> trim = new Multiset(Stream.of("a", "a", "a", "b", "b").collect(Collectors.toList()));
        
        assertEquals(trim, multiset.trim(2));
    }
    
    @Test
    public void subsetTest()
    {
        Multiset<String> multiset = new Multiset(Stream.of("a", "a", "a", "b", "b", "c").collect(Collectors.toList()));
        
        Multiset<String> subset = new Multiset(Stream.of("a", "a").collect(Collectors.toList()));
        Multiset<String> noSubset = new Multiset(Stream.of("a", "a", "c", "c").collect(Collectors.toList()));
        
        assertEquals(true, subset.isSubsetOf(multiset));
        assertEquals(true, multiset.isSubsetOf(multiset));
        assertEquals(false, noSubset.isSubsetOf(multiset));
    }
}
