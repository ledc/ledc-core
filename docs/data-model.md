# Data model of ledc-core

The different modules available in ledc use a simple yet flexible data model.
This central entities in this data model are [DataObject](#dataobject) and [Dataset](#dataset).
When data resides in a file or in a database, ledc-core offers a few basic binding abilities to read/write Datasets.
In it's current version, data can be read from and written to a [Postgres database via JDBC](#jdbc-binding) and a [csv](#csv-binding) file.

## DataObject

### Datatypes

A **DataObject** is basically an object that describes an entity in the real world.
It is a simple key-value map, where there permitted values are restricted to a few common datatypes.
The permitted datatypes are shown in the table below.

| Name       | Description                                                               |
|------------|---------------------------------------------------------------------------|
| String     | Textual data                                                              |
| Integer    | 4-bytes integer number                                                    |
| Long       | 8-bytes integer number                                                    |
| Boolean    | Boolean                                                                   |
| Float      | 4-bytes floating point decimal                                            |
| Double     | 8-bytes floating point decimal                                            |
| BigDecimal | [Fixed point decimal with custom scale](https://docs.oracle.com/javase/8/docs/api/java/math/BigDecimal.html)|
| Date       | [Date](https://docs.oracle.com/javase/8/docs/api/java/time/LocalDate.html)|
| Time       | [Time](https://docs.oracle.com/javase/8/docs/api/java/time/LocalTime.html)|
| Datetime   | [Timestamp](https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html)|
| Duration   | [An amount of time](https://docs.oracle.com/javase/8/docs/api/java/time/Duration.html)|
| Instant    | [A point on the timeline](https://docs.oracle.com/javase/8/docs/api/java/time/Instant.html)|
| List       | A list of objects                                                         |
| DataObject | A DataObject                                                              |

DataObjects can be created as empty objects, to which attributes and values can be added.
An example of creating a DataObject is shown below.

```java
DataObject o = new DataObject()
    .setInteger("a", 4)
    .setString("b", "Hello world!")
    .setDate("c", LocalDate.of(2021, 6, 18));
```

By default, attribute names are case-insensitive and names will be converted to lowercase.
If you don't want this, you can pass a flag in the constructor.

```java
DataObject o = new DataObject(true).setBoolean("NowItsCaseSensitive", true);
```

### Operators
Besides the whole range of attribute getters (e.g., "getString"), setters (e.g., "setString") and type checkers (e.g., "isString"), there are a few interesting operations on objects.

First of all, there is a range of operators to enable datatype conversion for individual attributes.
Not all datatypes can be converted to one another and when conversion is not possible, a DataException will be thrown.
The table below summarizes the conversion rules between datatypes.

| From\To    | String   | Integer    | Long       | Boolean    | Float      | Double     | BigDecimal | Date      | Time  | Datetime  |
|------------|----------|------------|------------|------------|------------|------------|------------|-----------|-------|-----------|
| String     | Yes      | parse      | parse      | parse      | parse      | parse      | parse      | parse     | parse |   parse   |
| Integer    | toString | Yes        | Yes        | 0 is False | Yes        | Yes        | Yes        | No        | No    | No        |
| Long       | toString | Yes        | Yes        | 0 is False | Yes        | Yes        | Yes        | No        | No    | No        |
| Boolean    | toString | 0 is False | 0 is False | Yes        | 0 is False | 0 is False | 0 is False | No        | No    | No        |
| Float      | toString | Rounded    | Rounded    | 0 is False | Yes        | Yes        | Yes        | No        | No    | No        |
| Double     | toString | Rounded    | Rounded    | 0 is False | Yes        | Yes        | Yes        | No        | No    | No        |
| BigDecimal | toString | Rounded    | Rounded    | 0 is False | Yes        | Yes        | Yes        | No        | No    | No        |
| Date       | toString | No         | No         | No         | No         | No         | No         | Yes       | No    | Truncated |
| Time       | toString | No         | No         | No         | No         | No         | No         | No        | Yes   | No        |
| Datetime   | toString | No         | No         | No         | No         | No         | No         | Truncated | No    | Yes       |

Conversions can be made by calling functions starting with "as" followed by a datatype, as is illustrated below.

```java
DataObject o = new DataObject().set("a", "4");

//Convert a to integer
o.asInteger("a");
```

Attributes can be renamed by means of the **rename** function.
The new name is constrained in the sense that you cannot choose the name of an attribute that already exists.
So when renaming, it is not permitted to overwrite attributes.

The **projection** of an object over some of it's attributes.
This will return a new DataObject that contains only the attributes specified in the projection.
In a similar spirit, **inverse projection** returns a new DataObject that keeps only attributes *not* specified.
```java
DataObject o = new DataObject()
    .setInteger("a", 4)
    .setString("b", "Hello world!")
    .setDate("c", LocalDate.of(2021, 6, 18));
    
//Will keep attributes a and b    
DataObject projection = o.project("a", "b"); 

//Will keep attributes a and c    
DataObject inverseProjection = o.inverseProject("b"); 
```

The **concatenation** of two DataObjects is a new DataObject obtained by taking the first object and setting all attributes of the second object.
In particular, if both objects share attributes, the values of the second object will be chosen.
So keep in mind concatenation is not symmetric if the both objects have attributes in common.
Also note that for concatentation, the result is case-sensitive only if both input objects are case-sensitive.

```java
DataObject o1 = new DataObject(true)
    .setInteger("a", 4)
    .setString("b", "Hello world!")
    
DataObject o2 = new DataObject()    
    .setString("b", "Let there be change");
    .setDate("c", LocalDate.of(2021, 6, 18));
    
//Will result in
// a=4
// b="Let there be change"
// c = LocalDate.of(2021, 6, 18)
// and will be not case-sensitive (because o2 is not)
DataObject concat = o1.concat(o2); 
```

In case you want to implement merging operators on datasets, it will be convenient to remove redundant objects.
To support that, two commonly used concepts are **subsumption** and **complementation**.
A DataObject `a` *subsumes* another DataObject `b` if for all attributes we have that either that (i) `a` and `b` have the same value or (ii) `b` has a null value.
Subsumption thus expresses that one DataObject is *dominant* to another when it comes to the information it provides.
In a similar way, a DataObject `a` *complements* another DataObject `b` if for all attributes we have that either that (i) `a` and `b` have the same value or (ii) one of them has a null value.
Complementation thus expresses that two DataObjects can be easily merged into a new DataObject that subsumes the two original objects.
Both of these tests are available via the methods `subsumes` and `complements`.

## Dataset

DataObjects can be bundled in a *Dataset*.
A Dataset is an interface that offers a series of operations on collections of DataObject and has currently a few basic implementations that are interlinked as shown below.
The diagram also shows the available operations for each type of dataset and the constraints it enforces.

```mermaid
graph TB
    dataset("Dataset");
    simple("SimpleDataset");
    contracted("ContractedDataset");
    fixedtype("FixedTypeDataset");
    
    dataset-->simple;
    dataset--"Add constraints"-->contracted;
    contracted--"Enforces equi-type attributes"-->fixedtype;
    
    subgraph Operations TB
        basico[addDataObject<br>removeDataObject<br>replaceDataObject<br>getDataObjects<br>getSize<br>datatype conversion<br>project<br>inverseProject<br>select<br>head<br>tail<br>unionAll<br>union<br>intersect<br>sort<br>distinct];
        contro[naturalJoin<br>groupByAndAggregate];
    end;
    
    dataset-.->basico;
    contracted-.->contro;
    
    style basico fill:#ffffff, stroke:#2bbde9;
    style contro fill:#ffffff, stroke:#2bbde9;
    
```
    
### Simple Dataset

The most basic implementation is a **SimpleDataset** which is backed by a list.
A SimpleDataset offers no guarantees of whatsoever about the DataObjects that it contains: it just provides a list of DataObjects and these objects can differ from each other on a per-instance basis.
In particular, there is no "schema" that can be assumed on attributes.

A SimpleDataset however comes with the basic operations of a Dataset.
In particular this means a few SQL-like operations: project/inverseProject to focuss on particular attributes (if available) and select to apply filters on the available objects.
Operations head and tail allow the maintain only the first K or last K DataObjects of the Dataset.
Sort returns a sorted version of the dataset based on a given Comparator.
Union, intersection and union all behave like their SQL-like counterparts.
Union will keep all objects from both datasets, while intersection keeps only common objects.
Both operators have a set-like behavior in the sense they merge duplicate objects.
Union all will not remove such duplicate objects and is thus a union with a multiset-like behavior.
And finally, distinct will remove any duplicate DataObjects in the dataset.

### Contracted Dataset

Whenever there is a need to ensure that DataObjects feature a precise set of attributes, a **ContractedDataset** is the better choice.
In addition, attributes are assigned a *contract* to enforce constraints on the values an attribute can take.
This could be to enforce a datatype of each attribute, but in general can be any predicate (or a Boolean combination of multiple predicates).
A ContractedDataset therefore has much affinity and structural similarities with a table in a relational database.
A ContractedDataset also provides additional operations: it is possible to apply a natural join (in the sense of relational algebra) on two ContractedDatasets.
This will result in a new ContractedDataset that holds all combinations of DataObjects from both input datasets where equi-named attributes have equi-named values.
Next to that, it is also possible to apply grouping on a set of attributes that act as a group key and apply an aggregator on an additional attribute.

### Fixed-Type Dataset

A third implemenation of a dataset is a **FixedTypeDataset**.
This is basically a special kind of a ContractedDataset where all attributes *must* have the same datatype.
Fixed-Type datasets can be useful in algorithms where you know upfront that all attributes are equi-typed.
An example is where a DataObject is used to model a vector of reals.

## JDBC binding

If your data are stored in a [Postgres](https://www.postgresql.org/) database, you can use a JDBCBinder to specify the connection parameters of the Postgres instance.
A simple example of configuring a JDBCBinder is shown below.

```java
JDBCBinder mybinder = new JDBCBinder (new RelationalDB(
    DBMS.POSTGRESQL,
    "hostname",
    "port",
    "user",
    "password",
    "databasename",
    "schemaname")
);
```

### Reading data
A *JDBCDataReader* allows to build a Dataset object by passing on a SQL query and execute that query on a JDBC binding.
The easiest way to do so, is by the method `readData`.
```java
Dataset dataset = new JDBCDataReader(myBinder)
    .readData("select * from my_table");
```
The code above will instantiate (i) a SimpleDataset object and (ii) will read all data in memory at once.
If it is important that you know the datatypes of the individual attributes, then the method `readContractedData` is a better choice.
This will return a ContractedDataset, where each attribute is contracted to the type as found in the database.
```java
ContractedDataset dataset = new JDBCDataReader(myBinder)
    .readContractedData("select * from my_table");
```

If the amount of rows in the dataset is too large, then you can consider the method `readIterableData`.
With this method, you are given an Iterator over Dataset object.
In each step of the iteration, one chunk of data is presented.
You can specify the number of rows that are allowed in one chunk as a parameter of the method.

```java
Iterator<Dataset> datasetIterator = new JDBCDataReader(myBinder)
    .readIterableData("select * from my_table", 1000);
```

### Writing data

A *JDBCDataWriter* allows to write a Dataset object to a JDBC binding.
To configure a JDBCWriter, you need to specify a JDBCBinder as well as a target table in the database.
The latter should be a reference to a *TableSchema* object.
Such objects are created automatically by the binder when you ask for the schema of the database.
You also must indicate whether or not auto-generated keys (e.g. via sequences in PostGres) are used.
If this is the case, the writer object will collect these keys are return them upon writing time
```java
JDBCDataWriter writer = new JDBCDataWriter(
    myBinder,
    myBinder
        //Inspect the database metadata and build a database schema
        .getSchema() 
        //Retrieves from the schema a TableSchema object
        .get("target_table_name"), 
    // A Boolean that indicates if auto-generated keys will be used.
    false); 
```
Once a JDBCWriter is setup, you can pass a Dataset object to the `writeData` method.
Writing data will be done in batches, where only one commit is issued at the end of each batch.
You can configure the maximum size of a batch to control the writing process.
When auto-generated keys are used, they will be returned as a Dataset object.
The keys will be in the same order as they were generated.

```java
Dataset myDataset = new SimpleDataset();

// Add some objects
// ...

writer.setMaxBatchSize(10000);
Dataset generetedKeys = writer.writeData(myDataset);
writer.closeConnection();
```

## CSV binding

If your data are stored in a csv file, you can use a CSVBinder to specify how to treat data.
A simple example of configuring a CSVBinder is shown below.
Basically, you need to specify a File and some properties of the CSV binding like the presence of a header line, the separation string and a quote character.
Additional options are specifying a comment symbol, a set of symbols that should be treated as null values and an encoding Charset.
```java
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(
        true, //Header line
        ",",  //Comma-separated 
        '"', //Quoting character is "
        Stream.of("","null", "?").collect(Collectors.toSet()), //Symbols that will be treated as null
        "--", //Treat lines starting with -- as comment
        StandardCharsets.UTF_8 //UTF8 encoding
    )
    new File("myFile.csv"));
```

### Reading data

A *CSVDataReader* allows to build a Dataset object by reading data from CSVBinder.
This is done via the method `readData`.
This method will return a [ContractedDataset](#contracted-dataset) in which all attributes are contracted to the String datatype.

```java
ContractedDataset dataset = new CSVDataReader(csvBinder).readData(); 
```

If datatypes are important, the DataReader can make guesses on the datatypes, as illustrated in the following snippet of code. 
```java
int sampleSize = 1000;
ContractedDataset typedDataset = new CSVDataReader(csvBinder).readDataWithTypeInference(sampleSize);
```
In this code, the variable `sampleSize` indicates the amount of rows the inferencer will take from the head of the dataset to test it's guesses on. 

### Writing data
A *CSVDataWriter* allows to write a Dataset object to a CSVBinder.
The Dataset that is passed need not to be contracted to String datatypes, but a conversion to strings will be done when writing.

```java
new CSVDataWriter(csvBinder).writeData(myDataset); 
```



